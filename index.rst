Edison Integration documentation!
=======================================
The Energy Data and Intelligence System for Off-Grid Networks (EDISON) is a platform for tracking, and verification of renewable energy and offgrid-market progress of the Beyond the Grid Fund for Zambia (BGFZ).

To start using the POST API please, request for access token from the admin through either https://www.bgfz.org/contact-2/. The documentation covers the services offered below
 
 * Edison Stats (as seen in https://www.bgfz.org/) - no need for a token
 * Edison Energy Service Provider (ESP) Intgeration - need a token


Edison Stats
===============
This section describes the various stats shown on the live edison website and how you can query them using simple http GET request.

ESS Stats
----------------
Returns the total number of Energy Service Subscriptions sold

URL: https://edisonapi.bgfz.org:8443/edison-prod/statistics/ess

Method: ``GET``

See following example response:

.. code-block:: none

   {"payload":
   {"label":"ess",
   "count":152790,
   "description":"The total number of Energy Service Subscriptions sold"}
   }


Beneficiaries Stats
----------------

Returns the total number of beneficiaries

URL: https://edisonapi.bgfz.org:8443/edison-prod/statistics/beneficiaries

Method: ``GET``

See following example response:

.. code-block:: none

   {"payload":
      {"label":"beneficiaries",
      "count":794518,
      "description":"The total number of beneficiaries"}
   }

Jobs Stats
----------------

Returns the total number of Jobs

URL: https://edisonapi.bgfz.org:8443/edison-prod/statistics/jobs

Method: ``GET``

See following example response:

.. code-block:: none

   {"payload":
      {"label":"jobs",
      "count":1664.000000,
      "description":"The total number of jobs created"}
   }


Sellers Stats
----------------

Returns the total number of local sales agents recruited

URL: https://edisonapi.bgfz.org:8443/edison-prod/statistics/sellers

Method: ``GET``

See following example response:

.. code-block:: none

   {"payload":
      {"label":"sellers",
      "count":2706,
      "description":"The total number of local sales agents recruited"}
   }


Financial Stats
----------------

Returns the number additional investment leveraged in USD

URL: https://edisonapi.bgfz.org:8443/edison-prod/statistics/financial-leverage

Method: ``GET``

See following example response:

.. code-block:: none

   {"payload":
      {"label":"financial leverage",
      "count":35,
      "description":"The total amount of co-funding leveraged by ESPs in millions USD"}
   }

Co2 Stats
----------------

Returns the total number of kg of CO2 mitigated annually

URL: https://edisonapi.bgfz.org:8443/edison-prod/statistics/co2

Method: ``GET``

See following example response:

.. code-block:: none

   {"payload":
      {"label":"Kg of Co2 Mitigated Annually",
      "count":376706,
      "description":"The total number Kg of Co2 Mitigated Annually"}
   }


Solar Home Systems Integration
===============
Integrating Solar Home Systems requires an api ``POST`` token and only registered energy service providers can contribute data. 

URL: https://devedisonapi.bgfz.org:8443/edison-dev/external-data/shs-ess/<api token>

Method: ``POST``

SHS Data Post Example
----------------

.. code-block:: none

   [
  {
    "acquisitionDate": "2020-02-24T13:59:42.896Z",
    "beneficiary": {
      "cdAreaCode": 0,
      "extKey": "string",
      "nameCity": "string",
      "nameFirst": "string",
      "nameLast": "string",
      "nameVillage": "string",
      "partyType": "string",
      "textAddress": "string",
      "textEmail": "string",
      "textPhoneNumber": "string",
      "nameProvince": "Copperbelt",
      "textGender": "M"
    },
    "currentDaysOutstanding": 0,
    "customer": {
      "cdAreaCode": 0,
      "extKey": "string",
      "nameCity": "string",
      "nameFirst": "string",
      "nameLast": "string",
      "nameVillage": "string",
      "partyType": "string",
      "textAddress": "string",
      "textEmail": "string",
      "textPhoneNumber": "string",
      "nameProvince": "Copperbelt",
      "textGender": "M"
    },
    "datePaidOff": "2020-02-24T13:59:42.897Z",
    "daysPaid": 0,
    "daysToPay": 0,
    "downPayment": 0,
    "events": [
      {
        "extKey": "string",
        "textDescription": "string",
        "textEventType": "Warranty"
      }
    ],
    "extKey": "string",
    "minimumPaymentPerMonth": 0,
    "paygTransactions": [
      {
        "amtPayment": 0,
        "dtPayment": "2020-02-24T13:59:42.897Z",
        "extKey": "string",
        "textSms": "string",
        "textPaymentStatus": "Active",
        "textTransactionType": "Cash"
      }
    ],
    "planDuration": 0,
    "productId": "string",
    "repossessionDate": "2020-02-24T13:59:42.897Z",
    "seller": {
      "cdAreaCode": 0,
      "extKey": "string",
      "nameCity": "string",
      "nameFirst": "string",
      "nameLast": "string",
      "nameVillage": "string",
      "partyType": "string",
      "textAddress": "string",
      "textEmail": "string",
      "textPhoneNumber": "string",
      "nameProvince": "Copperbelt",
      "textGender": "M"
    },
    "serialNumber": "string",
    "totalDaysOutstanding": 0,
    "totalFinancedAtTimeOfPurchase": 0,
    "paymentType": "Cash",
    "productSubUse": "School",
    "productUse": "Household"
      }
   ]


.. csv-table:: Concept
   :header: "Attribute ", "Description"
   :widths: 35, 35

         "acquisitionDate", "Date of product purchase in YYYY-MM-DDT format (2020-02-24T13:59:42.896Z)"
         "beneficiary", "Information about the person benefiting from the energy services"
         "currentDaysOutstanding", "Number of days default"
         "datePaidOff", "The payment completion date"
         "daysPaid", "Information about the person benefiting from the energy services"
         "daysPaid", "Number of days paid for"
         "downPayment", "Initial payment made by the customer"
         "events", ""
         "extKey", "Unique Customer Ref"
         "minimumPaymentPerMonth", "Minimum Payment"
         "planDuration", "Length of customer Plan"
         "productId","Product Identification"
         "repossessionDate","Date of repossession if product is "
         "seller","Agent identification"
         "serialNumber", "Prodcut serial number"
         "totalDaysOutstanding", "Number of days default"
         "paymentType", "Payment method"
         "productUse", "What the product is used for"


         

 
.. Subject Subtitle
.. ----------------
.. Subtitles are set with '-' and are required to have the same length 
.. of the subtitle itself, just like titles.
 
.. Lists can be unnumbered like:
 
..  * Item Foo
..  * Item Bar
 
.. Or automatically numbered:
 
..  #. Item 1
..  #. Item 2
 
.. Inline Markup
.. -------------
.. Words can have *emphasis in italics* or be **bold** and you can define
.. code samples with back quotes, like when you talk about a command: ``sudo`` 
.. gives you super user powers!